// Simple requires.
var path = require('path');
var changeCase = require('change-case');
var fs = require('fs');
var pluralize = require('pluralize');
// Relative require - not node_module.
var app = require(path.resolve(__dirname, '../server'));
// Set datasource.
/*** EDIT THIS PER PROJECT ************************************************************************/
var ds = app.dataSources.pointsDS;
var schema = 'public';
/*** END EDIT THIS PER PROJECT ********************************************************************/
// Output path - requires subfolder named 'output' within bin folder.
var outputPath = path.resolve(__dirname, './output');

// Discover all tables in schema
ds.discoverModelDefinitions({views: false, schema: schema}, function (err, tables) {

  const HAS_MANY = 'hasMany';

  // If there was no error...
  if (!err) {
    var i = 0;

    /**
     * Queries for the foreign key relationships pointing at the table - i.e. the hasMany
     * relationships of the passed-in table.
     * @param table A table object from the array returned from discoverSchemas(), for which the
     * hasMany relationships are required.
     * @returns {Promise} A Promise which resolves upon the return of the asynch call to
     * discoverExportedForeignKeys().
     * The resolve() payload contains an array of hasMany relationships of the table.
     */
    var fetchHasManyRels = function (table) {
      return new Promise(function (resolve, reject) {
        ds.discoverExportedForeignKeys(table.name, function (err, res) {
          if (err) {
            reject(err);
          } else {
            resolve(res);
          }
        });
      });
    };

    /**
     * Void function to process each table.
     * The function is both recursive and iterative, in that its terminal condition is just hitting
     * the point at which there are no more tables to process.
     *
     * The only reason that it is recursive (calls itself) is so that the next loop iteration takes
     * place after the asynchronous callback from discoverSchemas() has completed - doing this as
     * tables.forEach(), for example, doesn't work since the next iteration begins before the
     * asynchronous actions initiated by the previous iteration have completed.
     */
    var processNext = function () {
      // Get the next table, starting at 0 in the array and moving forwards.
      var table = tables[i++];

      // If a table was found.
      if (table) {
        ds.discoverSchemas(table.name, {schema: schema, relations: true}, function (err, res) {
          // Singularise the model name.
          res = res[schema + '.' + table.name];
          res.name = pluralize(res.name, 1);
          // If there are any relations...
          if (res.options.relations) {
            var rels = res.options.relations;
            // For each relationship - i.e. key within 'relations'...
            for (var rel in rels) {
              if (rels.hasOwnProperty(rel)) {
                // Set model property to the singular version of that relation.
                rels[rel].model = pluralize(rels[rel].model, 1);
                /* Since it is belongsTo, set the reationship itself to singular
                 * (i.e. it is belongsTo one).
                 */
                rels[pluralize(rel, 1)] = rels[rel];
                // Delete plural version of the relationship.
                delete rels[rel];
              }
            }
          }
          // Change the singular of table name into param-case to match JS file-naming conventions.
          var outputName = outputPath+'/'+changeCase.paramCase(pluralize(table.name, 1))+'.json';

          // Call function which returns a promise to add hasMany relationships.
          fetchHasManyRels(table).then(function (hasManyRelsRes) {
            // Iterate over has many relationships, adding them to the main response.
            hasManyRelsRes.forEach(function (hasManyRel) {
              // Init req object properties if they don't already exist due to no belongsTo rels.
              if (!res.options) {
                res.options = {};
              }
              if (!res.options.relations) {
                res.options.relations = {};
              }
              // Add property for relation - table name in Camel Case. Leave plural since hasMany.
              var relName = changeCase.camelCase(hasManyRel.fkTableName);
              /* Set up value of relation property - an object with:
               * - model set to the singular of the relation name, in Pascal case.
               * - type to a constant value of 'hasMany'
               * - foreignKey to the camel case version of the foreign key column name.
               */
              res.options.relations[relName] = {
                model: changeCase.pascalCase(pluralize(relName, 1)),
                type: HAS_MANY,
                foreignKey: changeCase.camelCase(hasManyRel.fkColumnName)
              };
            });
            // Output to the ./output folder for review prior to copying to ../../common/models.
            console.log('Outputting: ' + outputName);
            fs.writeFileSync(outputName, JSON.stringify(res, null, 2), 'utf-8');
            processNext();
          }).catch(function (err) {
            console.error('Error adding hasMany relationships... ' + JSON.stringify(err));
            i++;
          });
        });
      } else {
        ds.disconnect();
      }
    };

    /* Initiate the first call of the function if there was at least one result.
     * The function is then responsible for calling itself until there are no more tables, and then
     * disconnecting from the data source.
     */
    if (tables && tables.length > 0) {
      processNext();
    } else {
      console.error('No tables found!');
    }
  } else {
    // If the process failed, display any error.
    console.error('Error: ' + JSON.stringify(err));
    try {
      ds.disconnect();
    } catch (e) {}
  }
});
