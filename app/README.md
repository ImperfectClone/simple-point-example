# simple-point-example-api

This subdirectory contains the API for the simple point example, 
created with [LoopBack](http://loopback.io).

## 1. Environment variables

The following environment variables are used within `server/datasources.json` 
to point the API at a particular database instance:

| Variable                     | Default in Docker   | Explanation            |
|:-----------------------------|:--------------------|:-----------------------|
| POINTS_DB_PORT_5432_TCP_ADDR | localhost | The IP address for the PostgreSQL host containing the Points database |
| POINTS_DB_PORT_5432_TCP_PORT | 5432      | The port on the PostgreSQL host which the instance containing the RDS database is listening on |
| POINTS_DB_NAME               | postgres  | The name of the Points database instance; defaults to the postgres database |
| POINTS_DB_USER               | postgres  | The username for the $POINTS_DB_USER database role; defaults to using the out-of-the-box postgres user |
| POINTS_DB_PASS               |           | The password for the $POINTS_DB_USER database role; since this is just an example, defaults to passwordless |

For Docker, these may be set as environment variables, provided on the 
command line to `docker run`, or left at the default values (if the supplied 
Docker database image is used with the default settings, or installed into 
an instance for which the default settings are valid).

For standalone installations, these environment variables must be set within 
the runtime shell prior to calling `node .` from within the project's `app` 
directory.

## 2. Running with Docker

The Docker image can be built and an instance or instances of it created 
individually - i.e. without creating both the database and app together, 
as occurs when following the `docker-compose` instructions in the 
[root README] (../README.md).

To create the image, from within the project root directory, run:

```sh
docker build -t points-app-img app
```

Then, to create a mutable service instance based on the image, connected to 
an individually created database image with `--name points-db` run:

```sh
docker run --name points-app -p 8082:8080 points-app-img
```

Note that the p flag can be changed to run on a different host port, for 
example `-p 80:8080` would expose the container port 80 via the runtime 
host's port 80 (assuming this port is available and the user has permissions 
to bind to it).

The running instance can be started, stopped, and removed with 
`docker stop points-app`, `docker-stop-points-app` and 
`docker-rm-points-app`.

The image can then be rebuilt to pick up new code changes with:
```sh
docker build --no-cache -t points-app-img app
```

### Providing Environment Variables

When creating an instance from an image using `docker run`, it is possible 
to supply values to the environment variables specified using the `ENV` 
instruction in the `Dockerfile`; for example, to override the 
`$POINTS_DB_USER`:

```sh
docker run -e POINTS_DB_USER='ImperfectClone' --name point-app -p 8082:8080 point-app-img
```

## 3. Running without Docker

For instructions on running standalone, see the 
[root README] (../README.md).