/* IIFE - do not add attributes to the global namespace *******************************************/
(function doInIsolatedNamespace(global) {

    /* Constants **********************************************************************************/
    var DEFAULT_LONG_LAT           = [-1.2616848, 51.7569059];
    var DEFAULT_MAP_ZOOM           = 7;
    var GET_MY_LOCATION_BTN_ID     = 'get-my-location';
    var WRITE_LOCATION_BTN_ID      = 'write-location';
    var POINT_MAP_DIV_ID           = 'point-map';
    var LONG_INPUT_ID              = 'long-in';
    var LAT_INPUT_ID               = 'lat-in';
    var LONG_INPUT_VALUE_DISP_ID   = 'long-in-val-disp';
    var LAT_INPUT_VALUE_DISP_ID    = 'lat-in-val-disp';
    var DESC_TEXTAREA_ID           = 'description-in';
    var GEOLOC_CAPABILITY_KEY      = 'geolocation';
    var POINTS_WS_URL              = location.protocol + '//' + location.hostname + (
                                       location.port ? ':' + location.port : ''
                                     ) + '/points-ex/api/Points';

    var MAP_POINT_IS_INVALID_ERR   = 'Could not write invalud map point to the database';
    var ILLEGAL_GETGEOLOC_CALL_ERR = 'getGeolocation called were geolocationIsAvailable is false';
    var LONG_LAT_NOT_NUM_ARRAY_ERR = 'long-lat must be provided as an array of two numbers';
    var MUST_BE_LONG_LAT_ARR_ERR   = 'The parameter to addPoints must be an array of one or more ' +
                                     'long-lats';

    /* Create a class for manipulating the map ****************************************************/

    /**
     * Map constructor, used to create instances of Map. Each instance wraps an OpenLayers map, and
     * exposes only the limited API required for this app.
     * @param {string} elemId The ID of the HTML element into which the map instance should be
     * rendered.
     * @param {number} [zoom=3] A number indicating the zoom level of the map.
     * @param {number[]} [center=[0, 0]] A numerical array of length two indicating the long lat on
     * which to initially center the map.
     * @returns {{setCenter: setCenter, addPoints: addPoints}} A new object exposing the API of the
     * Map instance.
     * @constructor
     */
    function Map(elemId, zoom, center) {

        // Set the initial zoom and center
        zoom        = zoom ? zoom : DEFAULT_MAP_ZOOM;
        center      = center ? center : DEFAULT_LONG_LAT;
        var base    = new ol.layer.Tile({
            name: 'base',
            source: new ol.source.OSM()
        });
        var pointsLayer;

        // Construct the OpenLayers map
        var model = new ol.Map({
            layers: [base],
            controls: ol.control.defaults({
                attributionOptions: ({
                    collapsible: false
                })
            }),
            target: elemId,
            view: new ol.View({
                center: ol.proj.fromLonLat(center),
                zoom: zoom
            }),
            interactions: ol.interaction.defaults({mouseWheelZoom: false})
        });

        /**
         * Set the center of the map.
         * @param center {number[]} The two-element number array representing the coordinates on
         * which to center.
         */
        function setCenter(center) {
            if (!center || !center.length || center.length !== 2) {
                console.error('Cannot set center to anything except a two-element numerical array');
            } else {
                model.getView().setCenter(ol.proj.fromLonLat(center));
            }
        }

        /**
         * Add an array of points to the map.
         * @param {object[]} points An array of objects from LoopBack.
         */
        function addPoints(points) {

            // Remove any existing points layer
            model.removeLayer(pointsLayer);

            if (!points || !points.length) {
                console.error(MUST_BE_LONG_LAT_ARR_ERR);
            } else {
                var pointFeatures = [];
                points.forEach(function doForPoint(point) {
                    var longLatPair      = [point.long, point.lat];
                    var webMercatorPoint = ol.proj.fromLonLat(longLatPair);
                    var pointFeature     = new ol.Feature({});

                    pointFeature.setGeometry(new ol.geom.Point(
                        [webMercatorPoint[0], webMercatorPoint[1]]
                    ));
                    pointFeature.description = point.description;
                    pointFeatures.push(pointFeature);
                });

                var vectorLayer = new ol.layer.Vector({
                    name: 'points',
                    source: new ol.source.Vector({
                        features: pointFeatures
                    })
                });

                vectorLayer.setStyle(styleFunction());
                pointsLayer = vectorLayer;

                model.addLayer(pointsLayer);
            }
        }

        /**
         * Adds or removes the description label.
         * @param {string} text Flag indicating whether or not to show the label.
         * @returns {ol.style.Style} The style for the point - with the label added or removed/not
         * added.
         */
        function styleFunction(text) {

            var fill = new ol.style.Fill({
                color: [180, 0, 0, 0.8]
            });

            var stroke = new ol.style.Stroke({
                color: [180, 0, 0, 1],
                width: 1
            });

            var textFill = new ol.style.Fill({
                color: [180, 0, 0, 0.8]
            });

            var textStroke = new ol.style.Stroke({
                color: [0, 0, 0, 1],
                width: 1
            });

            var style = new ol.style.Style({
                image: new ol.style.Circle({
                    fill: fill,
                    stroke: stroke,
                    radius: 4
                }),
                fill: fill,
                stroke: stroke,
                text: new ol.style.Text({
                    font: '12px Arial',
                    text: undefined,
                    fill: textFill,
                    stroke: textStroke,
                    offsetY: -20
                })
            });

            if (text) {
                style.getText().setText(text);
            } else {
                style.getText().setText();
            }

            return style;
        }

        model.on('pointermove', function onPointerMove(evt) {
            if (evt.dragging) {
                return;
            }
            pointsLayer.getSource().getFeatures().forEach(function doForFeature(f) {
                f.setStyle(styleFunction());
            });
            var pixel = model.getEventPixel(evt.originalEvent);
            model.forEachFeatureAtPixel(pixel,function doForFeature(feature) {
                feature.setStyle(styleFunction(feature.description));
                return feature;
            });

        });

        // Return the API of the closured object
        return {
            setCenter: setCenter,
            addPoints: addPoints
        }
    }

    /* Create a class for storing the focused row *************************************************/

    /**
     * Create a new MapPoint instance, which represents a long-lat.
     * @param {number[]} longLat The longLat array.
     * @constructor
     */
    function MapPoint(longLat) {

        var description = '';

        /**
         * Set the description associated with the MapPoint.
         * @param {string} desc The description.
         */
        function setDescription(desc) {
            description = '' + desc;
        }

        /**
         * Set the long-lat for the MapPoint.
         * @param {number[]} newLongLat A numerical array of length 2, in which the first, longitude,
         * element must be within the range -180 to 180, and the second, latitude, element within the
         * range -90 to 90.
         */
        function setLongLat(newLongLat) {
            if (
                longLat.constructor !== Array || longLat.length !== 2 ||
                isNaN(longLat[0]) || isNaN(longLat[1])
            ) {
                console.error(LONG_LAT_NOT_NUM_ARRAY_ERR);
            } else {
                newLongLat[0] = +newLongLat[0];
                newLongLat[1] = +newLongLat[1];
                longLat = newLongLat;
            }
        }

        /**
         * Check that the MapPoint is valid for writing to the API.
         * @returns {boolean} True if the MapPoint instance is valid.
         */
        function isValid() {
            return !!(description && longLat && longLat.constructor === Array &&
            longLat.length === 2 && typeof longLat[0] === 'number' &&
            typeof longLat[1] === 'number' && longLat[0] >= -180 &&
            longLat[0] <= 180 && longLat[1] >= -90 && longLat[1] <= 90);
        }

        /**
         * Get the description for the MapPoint.
         * @returns {string} The description of this MapPoint.
         */
        function getDescription() {
            return description;
        }

        /**
         * Get the long-lat for the MapPoint.
         * @returns {number[]} The long-lat pair for this MapPoint.
         */
        function getLongLat() {
            return longLat;
        }

        // Return the API of the closured object
        return {
            getDescription: getDescription,
            getLongLat: getLongLat,
            setDescription: setDescription,
            setLongLat: setLongLat,
            isValid: isValid
        }
    }

    /* Geolocation functions **********************************************************************/

    /**
     * Method to check whether the environment is able to provide geolocation to the app.
     * @returns {boolean} True if geolocation is available within the browser, otherwise false
     */
    function geolocationIsAvailable() {
        return GEOLOC_CAPABILITY_KEY in global.navigator;
    }

    /**
     * Method to fecth the device's location via the global object API.
     * @param {function} onGeolocationFound Callback to be fired when geolocation is acquired.
     */
    function getGeolocation(onGeolocationFound) {
        if (geolocationIsAvailable()) {
            // Attempt to fetch a position
            global.navigator.geolocation.getCurrentPosition(function onPosFound(position) {
                // When the position is acquired, this callback fires and sets the lat long
                onGeolocationFound([position.coords.longitude, position.coords.latitude]);
            });
        } else {
            // Do nothing - but button to call this method should be disabled if loc not available
            console.error(ILLEGAL_GETGEOLOC_CALL_ERR);
        }
    }

    /**
     * Helper to be called to disable the get-my-location button where appropriate.
     */
    function disableGetMyLocationBtn() {
        document.getElementById(GET_MY_LOCATION_BTN_ID).disabled = true;
    }

    /* Back-end interaction functions *************************************************************/

    /**
     * Fetch the points currently in the database via the REST API.
     * @param {function} onPointsFetched Callback fired when the API returns a successful response.
     * Callback is passed the points as an argument.
     */
    function fetchPoints(onPointsFetched) {
        var pointsWebService = new XMLHttpRequest();

        pointsWebService.onreadystatechange = function onReadyStateChange() {
            if (this.readyState == 4 && this.status == 200) {
                onPointsFetched(JSON.parse(this.responseText));
            }
        };

        pointsWebService.open('GET', POINTS_WS_URL, true);
        pointsWebService.send();
    }

    /**
     * POST the new point to the REST API, requery, and reset the current point.
     */
    function pushPoint() {
        // Write the point out
        var pointsWebService = new XMLHttpRequest();
        var data = 'description=' + encodeURIComponent(mapPoint.getDescription()) + '&long=' +
            encodeURIComponent(mapPoint.getLongLat()[0]) + '&lat=' +
            encodeURIComponent(mapPoint.getLongLat()[1]);

        pointsWebService.onload = function onPointAdded() {
            // Requery
            fetchPoints(function onPointsFetched(points) {
                pointMap.addPoints(points);
            });
            // Reset the current point
            mapPoint = MapPoint(DEFAULT_LONG_LAT);
            updateView();
        };

        pointsWebService.open('POST', POINTS_WS_URL, true);
        pointsWebService.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        pointsWebService.send(data);
    }

    /* Event handlers *****************************************************************************/

    /**
     * Update the view based on the current state of the new map point input fields.
     */
    function updateView() {
        document.getElementById(DESC_TEXTAREA_ID).value = mapPoint.getDescription();
        document.getElementById(LONG_INPUT_ID).value    = mapPoint.getLongLat()[0];
        document.getElementById(LAT_INPUT_ID).value     = mapPoint.getLongLat()[1];
        manuallyFireOnChange([LONG_INPUT_ID, LAT_INPUT_ID]);
    }

    /**
     * Click-handler for get-my-location button.
     */
    function setLongLatToGeolocation() {
        getGeolocation(function onGeolocationFound(longLat) {
            // Center the map on the location
            pointMap.setCenter(longLat);
            // Set the long lat values to the location
            mapPoint.setLongLat(longLat);
            updateView();
        });
    }

    /**
     * Function to attach an on-change callback to one element which pushes that elements value to
     * another element.
     * @param {string} id The element ID to which to attach an on-change listener. The listener
     * will push the value of this element into the other element.
     * @param {string} otherId The ID of the element which should have its value set when the
     * on-change listener fires.
     */
    function addOnChangeToElemToPushValToOtherElem(id, otherId) {
        var elem = document.getElementById(id);
        elem.onchange=function onChange() {
            document.getElementById(otherId).innerHTML = elem.value;
        };
    }

    /**
     * Fire the onchange handler for an element via code.
     * @param {string[]} ids A list of IDs of the elements or element for which to fire an on-change
     * function.
     */
    function manuallyFireOnChange(ids) {
        ids.forEach(function forId(id) {
            document.getElementById(id).onchange();
        });
    }

    /**
     * Function checks whether the inputs set in the application are valid and, if they are, pushes
     * them into the database via the REST API.
     */
    function pushNewLocationIfValid() {
        // Set the description to the value in the text area
        mapPoint.setDescription(document.getElementById(DESC_TEXTAREA_ID).value);
        var newLongLat = [
            document.getElementById(LONG_INPUT_ID).value,
            document.getElementById(LAT_INPUT_ID).value
        ];
        mapPoint.setLongLat(newLongLat);

        // Check whether the instance is valid, and write it out if it is
        if (mapPoint.isValid()) {
            pushPoint();
        } else {
            console.error(MAP_POINT_IS_INVALID_ERR);
        }
    }

    /* Imperative init commands *******************************************************************/

    // Attach change event to long-lat inputs - propagate value selected via slider to span
    addOnChangeToElemToPushValToOtherElem(LONG_INPUT_ID, LONG_INPUT_VALUE_DISP_ID);
    addOnChangeToElemToPushValToOtherElem(LAT_INPUT_ID, LAT_INPUT_VALUE_DISP_ID);

    // Check whether geolocation if available and, if not, disable the button to fetch it
    if (!geolocationIsAvailable()) {
        disableGetMyLocationBtn();
    } else {
        // Attach click handler for geolocation fetch
        document.getElementById(GET_MY_LOCATION_BTN_ID).onclick=setLongLatToGeolocation;
    }

    // Attach click handler for push to API
    document.getElementById(WRITE_LOCATION_BTN_ID).onclick=pushNewLocationIfValid;

    // Ensure that initial slider values are reflected by values in associated spans
    manuallyFireOnChange([LONG_INPUT_ID, LAT_INPUT_ID]);

    // Do an initial fetch of the points in the database and add them to the map
    fetchPoints(function onPointsFetched(points) {
        pointMap.addPoints(points);
    });

    // Initialise the map
    var pointMap = Map(POINT_MAP_DIV_ID);
    // Initialise the new point (set via inputs)
    var mapPoint = MapPoint(DEFAULT_LONG_LAT);
    // Initialise the view fields to the values in the new MapPoint
    updateView();

})(this);
