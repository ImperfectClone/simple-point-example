# simple-point-example-utils

These helper scripts are not part of the application - but are of use as part of 
a Docker-based development workflow.

| Script                       | Explanation            |
|:-----------------------------|------------------------|
| docker-stop-conts.sh         | Stop all running service container instances |
| docker-rm-conts.sh           | Remove all stopped service container instances |
| docker-rmi.sh                | Remove all images not being used by extant service container instances (whether started or stopped) |

