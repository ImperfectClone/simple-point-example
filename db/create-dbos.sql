﻿BEGIN;

CREATE TABLE public.points (
  id SERIAL PRIMARY KEY,
  lat DOUBLE PRECISION NOT NULL,
  long DOUBLE PRECISION NOT NULL,
  description TEXT NOT NULL
);

COMMIT;