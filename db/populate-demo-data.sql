INSERT INTO public.points (lat, long, description)
     VALUES (51.771607, -1.284971, 'Port Meadow, Oxford');

INSERT INTO public.points (lat, long, description)
     VALUES (55.951820, -3.198094, 'Princes Street, Edinburgh');

INSERT INTO public.points (lat, long, description)
     VALUES (51.387085, -2.367404, 'Royal Crescent, Bath');

INSERT INTO public.points (lat, long, description)
     VALUES (51.451740, -2.600843, 'Bristol Unicorn');

INSERT INTO public.points (lat, long, description)
     VALUES (57.147690, -2.091423, 'Fittee, Aberdeen');

INSERT INTO public.points (lat, long, description)
     VALUES (54.226867, -2.773161, 'Milnethorpe');

INSERT INTO public.points (lat, long, description)
     VALUES (54.329533, -2.739609, 'Castle Crescent, Kendal');

INSERT INTO public.points (lat, long, description)
     VALUES (40.517355, -106.986776, 'Lake Windemere');