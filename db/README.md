# simple-point-example-db

This subdirectory contains the scripts to create the PostgreSQL database and 
populate it with demo data.

## 1. Running with Docker

The Docker image can be built and an instance or instances of it created 
individually - i.e. without creating both the database and app together, 
as occurs when following the `docker-compose` instructions in the 
[root README] (../README.md).

To create the image, from within the project root directory, run:

```sh
docker build -t points-db-img db
```

Then, to create a mutable service instance based on the image, run:

```sh
docker run --name points-db -p 5434:5432 points-db-img
```

Note that the p flag can be changed to run on a different host port, for 
example `-p 9999:5432` would expose the container port 5432 via the runtime 
host's port 9999.

The running instance can be started, stopped, and removed with 
`docker stop points-db`, `docker-stop-points-db` and 
`docker-rm-points-db`.

The image can then be rebuilt to pick up new code changes with:
```sh
docker build --no-cache -t points-db-img app
```

## 2. Running without Docker

For instructions on running standalone, see the 
[root README] (../README.md).