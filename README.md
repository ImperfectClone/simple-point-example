# simple-point-example
<mark>WIP!</mark>

This project demonstrates a very simple Web app for viewing and adding a set of 
points stored in a database on a map, without using a geoserver or any special 
geospatial data types.

## 1. Running with Docker

The following instructions describe using Docker (Linux containers) to run the 
project. The Docker images bundle the supporting software stack such that 
there is no need to install anything manually apart from Docker itself.

### Prerequisites

The following prerequisites are required to run the project with Docker:

1. Install a [Git client on the runtime host] (https://git-scm.com/downloads)
2. Install docker by [following the instructions for the runtime platform] (https://docs.docker.com/engine/installation/)

### Clone Git Repository

Clone the git repository into the location of choice from a terminal (Linux, 
MacOS) or Git Bash (Windows):

```sh
git clone https://gitlab.com/ImperfectClone/simple-point-example.git
```

### Forward Ports via VirtualBox (Mac and Windows only)

On Linux, Docker runs natively, so there is no need to do anything.

On MacOS and Windows, Docker installation creates a VM - to which any service 
ports which should be externally available must be forwarded.

If following the ports used in the documentation, the database container will 
map to host port 5434 and the application to host port 8082, so the 
configuration would look as follows (leave any existing SSH entry intact):

![Port forwarding] (portforward.png)

Note that once these are forwarded, the ports to the VM, requests to localhost 
will be forwarded to this VM, meaning that any server processes binding to 
these ports outside of the VM will not be accessible via localhost until 
the forwards are removed. **The host ports used in the Docker examples are 
therefore intentionally different from those used standalone, so that it is 
possible to switch between running standalone and within Docker without 
having to add and remove forwards to Virtualbox**


### Build and Run with Docker Compose

The docker-compose tool packaged with the Docker Engine can be used to 
orchestrate multiple Docker containers - in this case the PostgreSQL database 
container and the NodeJS container encapsulating the API and front-end.

Change directory into the git repository from within a terminal (Docker 
terminal on Windows and Mac; a regular terminal on Linux), and simply issue:

```
docker-compose up
```

This will expose the app on `8082` and the database on `5434`.

To edit the exposed ports, change the number before the colon in the `ports` 
attributes within the `docker-compose.yml` file.

To change the connection details used by the application to find the database, 
edit the values under `services->points-app->environment`.

For a table describing the available environment variables, see the 
[application README] (app/README.md).

### Cleanup

The `utils` directory contains helper scripts for working with docker.

To stop all containers:
`. utils/docker-stop-conts.sh`

To remove all containers:
`. utils/docker-rm-conts.sh`

To remove all images:
`. utils/docker-rmi.sh`

Running the above three scripts from within a Docker terminal will clean the 
slate of existing Docker state.

## 2. Running without Docker

The following instructions describe running the project manually, without 
using the Docker images which bundle the supporting software stack.

### Prerequisites

The following prerequisites are required to run the project stand-alone on a 
non-Docker host:

1. Install a [Git client on the runtime host] (https://git-scm.com/downloads)
2. Install [PostgreSQL 9.5 for the runtime platform] (https://wiki.postgresql.org/wiki/Detailed_installation_guides), 
optionally ensuring that the pgAdmin graphical client is also installed (this 
may or may not be bundled, depending on the platform and installer chosen) 
3. Install [NodeJS for the runtime platform] (https://nodejs.org/en/download/)

### Clone Git Repository

Clone the git repository into the location of choice from a terminal (Linux, 
MacOS) or Git Bash (Windows):

```sh
git clone git@gitlab.com:ImperfectClone/simple-point-example.git
```

### Run Database in PostgreSQL

The following steps are required to get the database schema set up within a 
standalone PostgreSQL instance.

#### Execute SQL Scripts

Connect to a database in the PostgreSQL instance - either a newly-created one 
or the default `postgres` database - and run the following SQL scripts via a 
Postgres client:

1. `db/create-dbos.sql` - will create the database table
2. `db/populate-demo-data.sql` - will populate the database table with some 
initial points.

#### Test Database

Having connected to the database with some client, the following SQL should
return the initial rows:

`SELECT * FROM points;`

### Run Web Application in Node

The following steps are required to get the Web application running locally 
within NodeJS.

#### Set Environment variables

Running standalone within NodeJS, all environment variables must be set, as 
there are no defaults; for example:

```sh
export POINTS_DB_PORT_5432_TCP_ADDR=127.0.0.1
export POINTS_DB_PORT_5432_TCP_PORT=5434
export POINTS_DB_NAME=postgres
export POINTS_DB_USER=postgres
export POINTS_DB_PASS=''
```

#### Install Node Modules

Dependencies for the API project and its contained front-end project must be 
installed with `npm`; from within the `/app` directory:

`npm install && npm --prefix ./client install ./client`

#### Initiate Server Process

From within the `/app` directory:

`node .`

#### Test API

By default, the API should become available for testing via a Web interface at:

`http://localhost:8080/points-ex/explorer/`

And the API proper, returning JSON, should be available via the pattern 
illustrated by the following example, a GET for all points:

`http://localhost:8080/points-ex/api/Points`

#### Test Web Application

The Web application should become available at the domain root:

`http://localhost:8080/`